.PHONY: clean
clean:
	@rm -rf build

.PHONY: build
build: build/webstore
	@scripts/build.sh

build/webstore:
	@mkdir -p build/webstore

