package main

import (
	"webstore/internal/webstore/startup"
)

func main() {
	startup.Do()
}
