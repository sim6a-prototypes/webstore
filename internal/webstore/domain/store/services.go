package store

func NewProductService(productRepository ProductRepository) ProductService {
	return ProductService{
		productRepository: productRepository,
	}
}

type ProductService struct {
	productRepository ProductRepository
}

func (s ProductService) CreateProduct(p Product) error {
	aProduct, err := s.productRepository.GetByName(p.Name)
	if err == nil {
		if aProduct.Price == p.Price && aProduct.Name == p.Name {
			return ErrProductAlreadyExists
		}
	} else if err != ErrProductNotFound {
		return err
	}

	if err := s.productRepository.Add(p); err != nil {
		return err
	}

	return nil
}
