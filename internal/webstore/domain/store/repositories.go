package store

type ProductRepository interface {
	Add(p Product) error

	GetByName(name Name) (Product, error)
}
