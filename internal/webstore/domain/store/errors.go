package store

import "errors"

var (
	ErrProductNotFound      = errors.New("a product not found")
	ErrProductAlreadyExists = errors.New("a product is already exists")
)
