package store

import "webstore/internal/webstore/domain/shared/money"

type Name string

type Product struct {
	Name  Name
	Price money.Money
}

type Products []Product
