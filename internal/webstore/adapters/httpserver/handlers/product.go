package handlers

import (
	"webstore/internal/webstore/restapi/product"

	"github.com/gofiber/fiber/v2"
)

func NewProductHandler(service product.Service) ProductHandler {
	return ProductHandler{
		service: service,
	}
}

type ProductHandler struct {
	service product.Service
}

func (h ProductHandler) CreateProduct(c *fiber.Ctx) error {
	productCreateСommand := product.CreateCommand{}
	if err := c.BodyParser(&productCreateСommand); err != nil {
		return err
	}

	if err := h.service.Create(productCreateСommand); err != nil {
		return err
	}

	return nil
}

func (h ProductHandler) ListProducts(c *fiber.Ctx) error {
	products, err := h.service.List()
	if err != nil {
		return err
	}

	return c.JSON(&products)
}
