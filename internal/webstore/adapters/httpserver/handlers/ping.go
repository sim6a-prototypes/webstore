package handlers

import "github.com/gofiber/fiber/v2"

type PingHandler struct {
}

func (h PingHandler) Ping(c *fiber.Ctx) error {
	return c.SendString("Pong")
}
