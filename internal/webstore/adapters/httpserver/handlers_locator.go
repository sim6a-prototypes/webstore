package httpserver

import "webstore/internal/webstore/adapters/httpserver/handlers"

type HandlersLocator struct {
	PingHandler    handlers.PingHandler
	ProductHandler handlers.ProductHandler
}
