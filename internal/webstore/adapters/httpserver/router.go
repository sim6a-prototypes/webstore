package httpserver

import "github.com/gofiber/fiber/v2"

func setRoutes(app *fiber.App, handlersLocator HandlersLocator) {
	app.Static("/", "./public")

	app.Get("/ping", handlersLocator.PingHandler.Ping)

	store := app.Group("/store")
	{
		store.Post("/products", handlersLocator.ProductHandler.CreateProduct)
		store.Get("/products", handlersLocator.ProductHandler.ListProducts)
	}

}
