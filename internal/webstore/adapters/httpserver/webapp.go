package httpserver

import (
	"github.com/gofiber/fiber/v2"
)

func NewWebApp(handlersLocator HandlersLocator) *fiber.App {
	app := fiber.New()
	setRoutes(app, handlersLocator)

	return app
}
