package product

import (
	"webstore/internal/webstore/domain/store"
	"webstore/internal/webstore/restapi/product"
)

func mapProductToDTO(p store.Product) product.DTO {
	return product.DTO{
		Name:  string(p.Name),
		Price: float64(p.Price),
	}
}

func mapProductsToDTO(products store.Products) product.ProductsDTO {
	dtos := make(product.ProductsDTO, 0, len(products))

	for _, p := range products {
		dtos = append(dtos, mapProductToDTO(p))
	}

	return dtos
}
