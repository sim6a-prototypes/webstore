package product

import (
	"sync"

	"webstore/internal/webstore/domain/store"
	"webstore/internal/webstore/restapi/product"
)

func NewInMemoryStorage() InMemoryStorage {
	return InMemoryStorage{}
}

type InMemoryStorage struct {
	sync.RWMutex

	products store.Products
}

func (s *InMemoryStorage) FindProducts() (product.ProductsDTO, error) {
	s.RLock()
	defer s.RUnlock()

	return mapProductsToDTO(s.products), nil
}

func (s *InMemoryStorage) Add(p store.Product) error {
	s.Lock()
	defer s.Unlock()

	s.products = append(s.products, p)

	return nil
}

func (s *InMemoryStorage) GetByName(name store.Name) (store.Product, error) {
	s.RLock()
	defer s.RUnlock()

	for _, p := range s.products {
		if p.Name == name {
			return p, nil
		}
	}

	return store.Product{}, store.ErrProductNotFound
}
