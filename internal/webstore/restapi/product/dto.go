package product

import "webstore/internal/webstore/domain/store"

type DTO struct {
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

type ProductsDTO []DTO

func newDTO(p store.Product) DTO {
	return DTO{
		Name:  string(p.Name),
		Price: float64(p.Price),
	}
}

func newProductsDTO(products store.Products) ProductsDTO {
	productsDTO := make(ProductsDTO, 0, len(products))

	for _, p := range products {
		productsDTO = append(productsDTO, newDTO(p))
	}

	return productsDTO
}
