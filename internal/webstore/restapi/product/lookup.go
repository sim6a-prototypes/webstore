package product

type ProductLookuper interface {
	FindProducts() (ProductsDTO, error)
}
