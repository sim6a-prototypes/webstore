package product

import (
	"webstore/internal/webstore/domain/shared/money"
	"webstore/internal/webstore/domain/store"
)

type CreateCommand struct {
	ProductName  string  `json:"name"`
	ProductPrice float64 `json:"price"`
}

func parseCreateCommand(command CreateCommand) store.Product {
	return store.Product{
		Name:  store.Name(command.ProductName),
		Price: money.Money(command.ProductPrice),
	}
}
