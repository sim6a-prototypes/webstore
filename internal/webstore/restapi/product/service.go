package product

import "webstore/internal/webstore/domain/store"

func NewService(
	productService store.ProductService,
	productLookuper ProductLookuper,
) Service {
	return Service{
		productService:  productService,
		productLookuper: productLookuper,
	}
}

type Service struct {
	productService  store.ProductService
	productLookuper ProductLookuper
}

func (s Service) List() (ProductsDTO, error) {
	return s.productLookuper.FindProducts()
}

func (s Service) Create(productCreateCommand CreateCommand) error {
	p := parseCreateCommand(productCreateCommand)

	return s.productService.CreateProduct(p)
}
