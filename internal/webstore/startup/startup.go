package startup

import (
	"os"
	"os/signal"
	"webstore/internal/webstore/adapters/httpserver"
	"webstore/internal/webstore/adapters/httpserver/handlers"
	inmemproductdb "webstore/internal/webstore/adapters/persistance/product"
	"webstore/internal/webstore/domain/store"
	"webstore/internal/webstore/restapi/product"

	"fmt"
)

func Do() {
	productStorage := inmemproductdb.NewInMemoryStorage()
	storeProductService := store.NewProductService(&productStorage)
	restProductService := product.NewService(storeProductService, &productStorage)

	handlersLocator := httpserver.HandlersLocator{
		PingHandler:    handlers.PingHandler{},
		ProductHandler: handlers.NewProductHandler(restProductService),
	}

	webService := httpserver.NewWebApp(handlersLocator)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		fmt.Println("Gracefully shutting down...")
		_ = webService.Shutdown()
	}()

	if err := webService.Listen(":8080"); err != nil {
		fmt.Println("web service error:", err)
	}
}
