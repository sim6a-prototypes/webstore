import { Component } from '@angular/core';

import { ProductService } from '../restapi/product.service';
import { Product } from '../restapi/product.model';
import { ProductStorageService } from './product-storage.service';

@Component({
    selector: 'new-product',
    templateUrl: './new-product.component.html'
})
export class NewProductComponent {

    productName: string = "";
    productPrice: number = 0;

    constructor(
        private productService: ProductService,
        private productStorage: ProductStorageService
    ) { }

    addProduct(name: string, price: number): void {
        let product = Product.parse(name, price);
        if (product.isEmpty()) {
            return;
        }

        this.productService.addProduct(product).
            subscribe(() => this.productStorage.products.push(product));
    }
}