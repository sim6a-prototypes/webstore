import { Component, OnInit } from '@angular/core';

import { ProductService } from '../restapi/product.service';
import { Product } from '../restapi/product.model';
import { ProductStorageService } from './product-storage.service';

@Component({
    selector: 'product-list',
    templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit {

    constructor(
        private productService: ProductService,
        private productStorage: ProductStorageService
    ) { }

    get products(): Product[] {
        return this.productStorage.products;
    }

    updateProducts() {
        this.productService.getProducts().
            subscribe((data: Product[]) => this.productStorage.products = data);
    }

    ngOnInit() {
        this.updateProducts();
    }
}
