import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NewProductComponent } from './new-product.component';
import { ProductListComponent } from './product-list.component';
import { ProductService } from '../restapi/product.service';
import { ProductStorageService } from './product-storage.service';


@NgModule({
    imports: [BrowserModule, FormsModule, HttpClientModule],
    declarations: [NewProductComponent, ProductListComponent],
    providers: [ProductService, ProductStorageService],
    exports: [NewProductComponent, ProductListComponent]
})
export class StoreModule { }