import { Injectable } from '@angular/core';

import { Product } from '../restapi/product.model';

@Injectable()
export class ProductStorageService {

    products: Product[] = [];

}