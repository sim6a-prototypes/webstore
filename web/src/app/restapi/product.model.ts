export class Product {
    constructor(public name: string, public price: number) { }

    static parse(name: string, price: number): Product  {
        if (name == null || name.trim() == "" || price == null) {
            return Product.empty();
        }

        return new Product(name, price);
    }

    static empty(): Product {
        return new Product("", 0);
    }

    isEmpty(): boolean {
        return this === Product.empty();
    }
}
