import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Product } from './product.model';

@Injectable()
export class ProductService {

    constructor(private httpClient: HttpClient) { }

    getProducts(): Observable<Product[]> {
        return this.httpClient.get<Product[]>('/store/products');
    }

    addProduct(product: Product): Observable<Product> {
        return this.httpClient.post<Product>('/store/products', product);
    }
}